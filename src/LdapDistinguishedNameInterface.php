<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Iterator;
use Stringable;

/**
 * LdapDistinguishedNameInterface interface file.
 *
 * This interface specifies how distinguished names should behave. This dn
 * is also an iterator that iterates over its components parts from the deepest
 * one to the shallower one. For example, if the dn is :
 * cn=foo,ou=bar,ou=qux,o=baz,c=fr , then the parts are 
 * [['c', 'fr'], ['o', 'baz'], ['ou', qux'], ['ou', 'bar'], ['cn', 'foo']]
 * and are iterated in this order, the c first and the cn last.
 *
 * @author Anastaszor
 * @extends \Iterator<LdapDistinguishedPartInterface>
 */
interface LdapDistinguishedNameInterface extends Iterator, Stringable
{
	
	/**
	 * Gets whether this distinguished name is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets the number of levels in this distinguished name. Zero is returned
	 * when this dn is empty.
	 * 
	 * @return integer
	 */
	public function getDepths() : int;
	
	/**
	 * Gets a new distinguished name that represents the dn of the parent. If
	 * this dn has only one depths or is already empty, an empty dn is returned.
	 * 
	 * @return LdapDistinguishedNameInterface
	 */
	public function getParentDistinguishedName() : LdapDistinguishedNameInterface;
	
	/**
	 * Gets a rfc compliant string representation of this dn.
	 * 
	 * @return string
	 */
	public function getStringRepresentation() : string;
	
	/**
	 * Gets the last field that is used in the distinguished name. An empty
	 * string is returned when this dn is empty.
	 * 
	 * @return string
	 */
	public function getFinalIdentifierField() : string;
	
	/**
	 * Gets the last value that is used in the distinguished name. An empty
	 * string is returned when the dn is empty.
	 * 
	 * @return string
	 */
	public function getFinalIdentifierValue() : string;
	
	/**
	 * Appends a combination of field and value to the current distinguished
	 * name, and returns a new distinguished name with the value appended at
	 * the end of the chain (most depths = beginning of the standard string
	 * representation for dn's). 
	 * 
	 * @param string $field
	 * @param string $value
	 * @return LdapDistinguishedNameInterface
	 */
	public function append(string $field, string $value) : LdapDistinguishedNameInterface;
	
	/**
	 * Appends a new dn part to the current distinguished name, and returns a
	 * new distinguished name with the value appended at the end of the chain.
	 * 
	 * @param LdapDistinguishedPartInterface $part
	 * @return LdapDistinguishedNameInterface
	 */
	public function appendPart(LdapDistinguishedPartInterface $part) : LdapDistinguishedNameInterface;
	
	/**
	 * Gets whether this distinguished name contains the other distinguished
	 * name, meaning the other distinguished name is included into this one.
	 * A dn is included in another if their intersection is equal to the
	 * included dn.
	 * 
	 * @param LdapDistinguishedNameInterface $other
	 * @return boolean
	 */
	public function contains(LdapDistinguishedNameInterface $other) : bool;
	
	/**
	 * Gets whether this distinguished name equals another distinguished name.
	 * Two dn's are equals if they have the same fields with the same values
	 * in the same order.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Intersects this distinguished name with the other distinguished name
	 * and returns the maximal overlap between the two dn, meaning they have
	 * the same fields and values in the right order, from the level 1 up to
	 * a certain level. If both dns do not match, their intersection is empty.
	 * 
	 * @param LdapDistinguishedNameInterface $other
	 * @return LdapDistinguishedNameInterface
	 */
	public function intersect(LdapDistinguishedNameInterface $other) : LdapDistinguishedNameInterface;
	
	/**
	 * Truncates this distinguished name at the given level and returns a
	 * distinguished name that have at most $levels. Truncating to zero or 
	 * negative levels leaves an empty dn.
	 * 
	 * @param integer $level
	 * @return LdapDistinguishedNameInterface
	 */
	public function truncate(int $level) : LdapDistinguishedNameInterface;
	
}
