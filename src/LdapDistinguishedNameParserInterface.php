<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use PhpExtended\Parser\ParserInterface;

/**
 * LdapDistinguishedNameParserInterface interface file.
 * 
 * This interface represents a parser for ldap distinguished names.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<LdapDistinguishedNameInterface>
 */
interface LdapDistinguishedNameParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
