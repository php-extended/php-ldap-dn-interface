<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapDistinguishedPartInterface interface file.
 * 
 * This interface specifies how a specific part should behave into a
 * distinguished name.
 * 
 * @author Anastaszor
 */
interface LdapDistinguishedPartInterface extends Stringable
{
	
	/**
	 * Gets the name of this part of dn. This must not be an empty string.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the value of this part of dn. This may be an empty string.
	 * 
	 * @return string
	 */
	public function getValue() : string;
	
	/**
	 * Gets a rfc compliant string representation of this dn part.
	 * 
	 * @return string
	 */
	public function getStringRepresentation() : string;
	
}
